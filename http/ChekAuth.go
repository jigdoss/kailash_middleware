package middleware

import (
	"net/http"
	jwt "github.com/dgrijalva/jwt-go"
	request "github.com/dgrijalva/jwt-go/request"
	"fmt"
	"context"
	"crypto/rsa"
	"io/ioutil"
)

func ChekAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := request.ParseFromRequest(r, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			} else {
				return getetPublicKey(), nil
			}
		})
		if err == nil && token.Valid {
			id := token.Claims.(jwt.MapClaims)["_id"]
			ctx := context.WithValue(r.Context(), "_id", id)
			next.ServeHTTP(w,r.WithContext(ctx))
			LogEntrySetField(r, "user_id", id)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
		}
	})
}
func getetPublicKey() *rsa.PublicKey {

	pubByte, err := ioutil.ReadFile("./jwtRS256.key.pub")

	rsaPub, err := jwt.ParseRSAPublicKeyFromPEM(pubByte)

	if err != nil {
		panic(err)
	}

	return rsaPub
}