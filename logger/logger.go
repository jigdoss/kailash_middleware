package logger

import "github.com/sirupsen/logrus"

func NewLogrus() *logrus.Logger {
	log := logrus.New()
	log.Formatter = &logrus.JSONFormatter{DisableTimestamp: true}
	return log
}
