package main

import (
	"github.com/go-chi/chi"
	"gitlab.com/jigdoss/kailash_middleware/http"
	"net/http"
	"time"
	"github.com/sirupsen/logrus"
)

func main()  {
	logger := logrus.New()
	logger.Formatter = &logrus.JSONFormatter{DisableTimestamp: true}

	// Routes
	r := chi.NewRouter()
	//r.Use(middleware.RequestID)
	r.Use(middleware.NewStructuredLogger(logger))
	r.Use(middleware.Recoverer)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome"))
	})
	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome"))
	})
	r.Get("/wait", func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(1 * time.Second)
		middleware.LogEntrySetField(r, "wait", true)
		w.Write([]byte("hi"))
	})
	r.Get("/panic", func(w http.ResponseWriter, r *http.Request) {
		panic("oops")
	})
	http.ListenAndServe(":3333", r)
}